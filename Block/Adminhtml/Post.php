<?php
namespace MilicaDev\CalculateInvoice\Block\Adminhtml;

class Post extends \Magento\Backend\Block\Widget\Grid\Container
{

	protected function _construct()
	{
		$this->_controller = 'adminhtml_post';
		$this->_blockGroup = 'MilicaDev_CalculateInvoice';
		$this->_headerText = __('Post');
		//$this->_addButtonLabel = __('Create New Post');
		parent::_construct();
		//$this->_removeButton = __('add new');
		 $this->buttonList->remove('add');
	}
}