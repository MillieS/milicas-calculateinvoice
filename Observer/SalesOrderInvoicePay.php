<?php
namespace MilicaDev\CalculateInvoice\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SalesOrderInvoicePay implements ObserverInterface
{   
/**
* @param EventObserver $observer
* @return $this
*/
	protected $helperData;
	protected $invoiceFactory;

	public function __construct(
		\MilicaDev\CalculateInvoice\Helper\Data $helperData,
		\MilicaDev\CalculateInvoice\Model\InvoiceFactory $invoiceFactory
	)
	{
	
		$this->helperData = $helperData;
		$this->invoiceFactory = $invoiceFactory;
	}
public function execute(EventObserver $observer)
{
     $invoice = $observer->getEvent()->getInvoice();
     $order = $invoice->getOrder();

     $orderid = $order->getId();
     $grandtotal = $invoice->getGrandTotal();
     $multiplier = $this->helperData->getGeneralConfig('display_number');
     

		if (preg_match('/^\\d+(\\.\\d+)?$/D', $multiplier)){
		    
		    $result = $grandtotal * $multiplier; 

		} else {
		    $multiplier = 1;
		    $result = $grandtotal * $multiplier; 

		}

     $dbtable = $this->invoiceFactory->create();
     $dbtable->setOrderId($orderid);
     $dbtable->setGrandTotal($grandtotal);
     $dbtable->setMultiplier($multiplier);
     $dbtable->setResult($result); 
     $dbtable->save();

     return;   
}    
}