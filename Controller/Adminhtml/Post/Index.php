<?php

namespace  MilicaDev\CalculateInvoice\Controller\Adminhtml\Post;

class Index extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;
	protected $helperData;

	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\MilicaDev\CalculateInvoice\Helper\Data $helperData
	)
	{
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
		$this->helperData = $helperData;
	}

	public function execute()
	{
		//echo $this->helperData->getGeneralConfig('enable');
		//echo $this->helperData->getGeneralConfig('display_number');
		$resultPage = $this->resultPageFactory->create();
		$resultPage->getConfig()->getTitle()->prepend((__('Posts')));
		

		return $resultPage;
	}


}