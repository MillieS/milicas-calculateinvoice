<?php

namespace MilicaDev\CalculateInvoice\Model;

class Invoice extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('MilicaDev\CalculateInvoice\Model\ResourceModel\Invoice');
    }
}