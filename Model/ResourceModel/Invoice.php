<?php

namespace MilicaDev\CalculateInvoice\Model\ResourceModel;

class Invoice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('milicadev_calculateinvoice', 'entity_id');
    }
}