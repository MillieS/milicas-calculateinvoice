<?php

namespace MilicaDev\CalculateInvoice\Model\ResourceModel\Invoice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'MilicaDev\CalculateInvoice\Model\Invoice',
            'MilicaDev\CalculateInvoice\Model\ResourceModel\Invoice'
        );
    }
}